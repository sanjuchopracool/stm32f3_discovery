import qbs 1.0
import qbs.FileInfo

Product {
    type: ["application", "hex", "bin", "size"]
    Depends { name: "cpp" }
    cpp.executableSuffix: ".out"
    cpp.executablePrefix: ""

    Rule {
        id: hex
        inputs: ["application"]
        prepare: {
            var args = ["-O", "ihex", input.filePath, output.filePath];
            var objcopyPath = input.cpp.objcopyPath
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to hex: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["hex"]
            filePath: FileInfo.baseName(input.filePath) + ".hex"
        }
    }

    Rule {
        id: bin
        inputs: ["application"]
        prepare: {
            var objcopyPath = input.cpp.objcopyPath
            var args = ["-O", "binary", input.filePath, output.filePath];
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to bin: "+ FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["bin"]
            filePath: FileInfo.baseName(input.filePath) + ".bin"
        }
    }

    Rule {
        id: size
        inputs: ["application"]
        alwaysRun: true
        prepare: {
            var sizePath = input.cpp.objcopyPath.slice(0, -7) + "size"
            var args = [input.filePath];
            var cmd = new Command(sizePath, args);
            cmd.description = "File size: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;
        }
        Artifact {
            fileTags: ["size"]
            filePath: undefined
        }
    }

    FileTagger {
        patterns: "*.ld"
        fileTags: ["linkerscript"]
    }

    cpp.cLanguageVersion: "c11"
    cpp.cxxLanguageVersion: "c++11"
    cpp.positionIndependentCode: false
    cpp.optimization: "none"
    cpp.debugInformation: true
    cpp.enableExceptions: false
    cpp.enableRtti: false
    cpp.enableReproducibleBuilds: true
    cpp.treatSystemHeadersAsDependencies: true

    cpp.driverFlags:
        [
        "-mcpu=cortex-m4",
        "-mthumb",
        "-mfloat-abi=softfp",
        "-mfpu=fpv4-sp-d16",
        "-Os",
        "-ffunction-sections",
        "-fdata-sections",
        "-nodefaultlibs",
        "-Wdouble-promotion",
        "-Wall",
//        "-flto"
    ]

    cpp.cxxFlags: [
    ]

    cpp.linkerFlags: [
        "--gc-sections",
        "-u,Reset_Handler"
    ]


    cpp.defines: [
        "STM32F303xC",
        "USE_FULL_ASSERT",
        "USE_STDPERIPH_DRIVER",
        "STM32F3DISCOVERY",
        "USE_FULL_ASSERT",
        "USE_SPI1",
    ]


    property  string libraryPath: "./Libraries"
    property string librarySourcesPath : libraryPath + "/STM32F30x_StdPeriph_Driver/src/"
    property string freeRtosSourcePath : "./freertos/Source/"
    cpp.includePaths:
        [
        ".",
        "./config",
        "./common",
        "./board",
        libraryPath + "/STM32F30x_StdPeriph_Driver/inc",
        libraryPath + "/CMSIS/Device/ST/STM32F30x/Include",
        libraryPath + "/CMSIS/Include",
    ]

    files: {
        var projectFiles = [
                    "readme.txt",
                    "./Config/*",
                    "common/system.c",
                    "common/printf.c",
                    "common/serial.c",
                    "common/SPI.cpp",
                    "common/L3GD20Gyro.cpp",
                    "board/Receiver.cpp",
                    "board/PWM.cpp",
                    "board/VoltageReader.cpp",
                    "ESC_CALIBRATION.cpp",
                ];

        var librarySources = [
                    "stm32f30x_rcc.c",
                    "stm32f30x_gpio.c",
                    "stm32f30x_usart.c",
                    "stm32f30x_spi.c",
                    "stm32f30x_tim.c",
                    "stm32f30x_misc.c",
                    "stm32f30x_exti.c",
                    "stm32f30x_syscfg.c",
                    "stm32f30x_adc.c",
                ]

        for(var i=0; i<librarySources.length; i++){
            projectFiles.push( librarySourcesPath + librarySources[i]);
        }

        return projectFiles;
    }

    cpp.staticLibraries:
        [
        "gcc",
        "c_nano",
        "m",
        "nosys",
    ]
}
