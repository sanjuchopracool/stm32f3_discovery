How to flash with stlink
st-flash write file.bin 0x8000000

Jlink

How to launch GDB Server
JLinkGDBServer -select USB -device STM32F429ZI -If SWD -speed auto -port 4242

Jlink script
---------------------------------
monitor halt
load
monitor reset
---------------------------------

without this script, code get stuck in constructors


NOTE:
For initialisation SPL code is fine, for runtime if required write code

GYRO Details: L3G4200D
Mode: SPI (SPI1, PA5, PA6, PA7)
WHO_AM_I: 0XD4 , 500dps
500 dps -> 17.5 mdps/digit

57.14286 = 1000/17.5
gyro*17.5/1000 = gyro/57.14286



PA1 -> ANALOG INPUT ADC1_IN2


INPUT CAPTURE FOR RECEIVER ( 5 inputs )
TIM2, TIM3 General purpose

PA2 -> TIM2_CH3
PA3 -> TIM2_CH4
PA4 -> TIM3_CH2
PA15 -> TIM2_CH1
PB4 -> TIM3_CH1



MOTOR OUT
PD12 -> TIM4_CH1
PD13 -> TIM4_CH2
PD14 -> TIM4_CH3
PD15 -> TIM4_CH4

Battery Safety
should not fly after 3.6 voltage

Voltage:
1.18 volt drop
2.61 at input
10.56 after drop

2.94 -> voltage regulator


ESC SimonK Firmware
Cells ? Everything is detected automatically

Setup Gyro Offset and direction:
pitch roll yaw
invert third axis ( yaw axis only)
Offsets:
-33.33, -30.80, 18.54


