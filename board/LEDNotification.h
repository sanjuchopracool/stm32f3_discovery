#ifndef LEDNOTIFICATION_H
#define LEDNOTIFICATION_H
#include <cstdint>

enum Error
{
    NO_ERROR,           // Green
    SETUP_ERROR,        // ALL
    LOW_BATTERY,        // Red
    RECEIVER_ERROR,     // Orange
    LOOP_TIME_EXPIRED,  // Orange
};

void initLeds();
void setError( uint8_t inError );
void error();
void toggleLED();

#endif // LEDNOTIFICATION_H
