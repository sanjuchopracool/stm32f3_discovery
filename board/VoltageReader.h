#ifndef VOLTAGEREADER_H
#define VOLTAGEREADER_H

#include <stdint.h>

class VoltageReader
{
public:
    VoltageReader();
    ~VoltageReader();

    void init();
    void deInit();

    uint16_t readVoltage();
};

#endif // VOLTAGEREADER_H
