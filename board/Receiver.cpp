#include "Receiver.h"
#include "stm32f30x.h"

void initReceiver()
{
    // GPIO Econfigure
    {
        GPIO_InitTypeDef gpioInit;
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

        gpioInit.GPIO_Pin =  GPIO_Pin_2 | //TIM2_CH3
                GPIO_Pin_3 |    //TIM2_CH4
                GPIO_Pin_4 |    //TIM3_CH2
                GPIO_Pin_15;    //TIM2_CH1

        gpioInit.GPIO_Mode = GPIO_Mode_AF;
        gpioInit.GPIO_Speed = GPIO_Speed_50MHz;
        gpioInit.GPIO_OType = GPIO_OType_PP;
        gpioInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
        GPIO_Init(GPIOA, &gpioInit);

        gpioInit.GPIO_Pin = GPIO_Pin_4;     //TIM3_CH1
        GPIO_Init(GPIOB, &gpioInit);

        // TIM2 -> AF1
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource15, GPIO_AF_1);

        // TIM3 -> AF2
        GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_2);
        GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_2);
    }

    // Enable TIM2 and TIM3 clock
    // both runs at 72 MHz
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);


    // Enable interrupts for Timer 2 and Timer 3
    {
        /* Enable the TIM2 global Interrupt */
        NVIC_InitTypeDef nvicInit;
        nvicInit.NVIC_IRQChannel = TIM2_IRQn;
        nvicInit.NVIC_IRQChannelPreemptionPriority = 0;
        nvicInit.NVIC_IRQChannelSubPriority = 0;
        nvicInit.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&nvicInit);

        /* Enable the TIM3 global Interrupt */
        nvicInit.NVIC_IRQChannel = TIM3_IRQn;
        NVIC_Init(&nvicInit);
    }

    // Base Clock Configuration
    {
        // TIM2 base clock
        TIM_TimeBaseInitTypeDef timeBaseInit;
        timeBaseInit.TIM_ClockDivision = TIM_CKD_DIV1;
        timeBaseInit.TIM_CounterMode = TIM_CounterMode_Up;
        timeBaseInit.TIM_Prescaler = 71;
        timeBaseInit.TIM_Period = 0xFFFF;
        timeBaseInit.TIM_RepetitionCounter = 0;
        TIM_TimeBaseInit( TIM2, &timeBaseInit );

        // TIM3 base clock
        TIM_TimeBaseInit( TIM3, &timeBaseInit );
    }

    // Input Capture
    {
        TIM_ICInitTypeDef  inputCaptureInit;
        inputCaptureInit.TIM_ICPolarity = TIM_ICPolarity_Rising;
        inputCaptureInit.TIM_ICSelection = TIM_ICSelection_DirectTI;
        inputCaptureInit.TIM_ICPrescaler = TIM_ICPSC_DIV1;
        inputCaptureInit.TIM_ICFilter = 0x0;

        inputCaptureInit.TIM_Channel = TIM_Channel_1;
        TIM_ICInit(TIM2, &inputCaptureInit);

        inputCaptureInit.TIM_Channel = TIM_Channel_3;
        TIM_ICInit(TIM2, &inputCaptureInit);

        inputCaptureInit.TIM_Channel = TIM_Channel_4;
        TIM_ICInit(TIM2, &inputCaptureInit);

        inputCaptureInit.TIM_Channel = TIM_Channel_1;
        TIM_ICInit(TIM3, &inputCaptureInit);

        inputCaptureInit.TIM_Channel = TIM_Channel_2;
        TIM_ICInit(TIM3, &inputCaptureInit);
    }

    //enable Timers
    {
        TIM_Cmd(TIM2, ENABLE);
        TIM_Cmd(TIM3, ENABLE);
    }

    //Configure Interrupts
    {
        TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);
        TIM_ITConfig(TIM2, TIM_IT_CC3, ENABLE);
        TIM_ITConfig(TIM2, TIM_IT_CC4, ENABLE);
        TIM_ITConfig(TIM3, TIM_IT_CC1, ENABLE);
        TIM_ITConfig(TIM3, TIM_IT_CC2, ENABLE);
    }
}

extern "C"
{

volatile int32_t channel1Rise;
volatile int32_t channel2Rise;
volatile int32_t channel3Rise;
volatile int32_t channel4Rise;
volatile int32_t channel5Rise;

volatile int32_t channel1;  //PA2
volatile int32_t channel2;  //PA3
volatile int32_t channel3;  //PA4
volatile int32_t channel4;  //PA15
volatile int32_t channel5;  //PB4

volatile int32_t temp;
void TIM2_IRQHandler(void)
{
    // channel4 -> CHANNEL 1, PA15
    if( TIM2->SR & TIM_IT_CC1)
    {
        if( GPIOA->IDR & GPIO_Pin_15 )
        {
            channel4Rise = TIM2->CCR1;
            TIM2->CCER |= TIM_CCER_CC1P;
        }
        else
        {
            temp = TIM2->CCR1 - channel4Rise;
            TIM2->CCER &= ~TIM_CCER_CC1P;
            if( temp < 0 ) temp += 0xFFFF;
            channel4 = temp;
        }
        TIM2->SR &= ~(TIM_IT_CC1);
    }

    // channel1 -> CHANNEL 3, PA2
    if( TIM2->SR & TIM_IT_CC3)
    {
        if( GPIOA->IDR & GPIO_Pin_2 )
        {
            channel1Rise = TIM2->CCR3;
            TIM2->CCER |= TIM_CCER_CC3P;
        }
        else
        {
            temp = TIM2->CCR3 - channel1Rise;
            TIM2->CCER &= ~TIM_CCER_CC3P;
            if( temp < 0 ) temp += 0xFFFF;
            channel1 = temp;
        }
        TIM2->SR &= ~(TIM_IT_CC3);
    }

    // channel2 -> CHANNEL 4, PA3
    if( TIM2->SR & TIM_IT_CC4)
    {
        if( GPIOA->IDR & GPIO_Pin_3 )
        {
            channel2Rise = TIM2->CCR4;
            TIM2->CCER |= TIM_CCER_CC4P;
        }
        else
        {
            temp = TIM2->CCR4 - channel2Rise;
            TIM2->CCER &= ~TIM_CCER_CC4P;
            if( temp < 0 ) temp += 0xFFFF;
            channel2 = temp;
        }
        TIM2->SR &= ~(TIM_IT_CC4);
    }
}


// Timer3 Interrupt
void TIM3_IRQHandler(void)
{
    // channel5 -> CHANNEL 1, PB4
    if( TIM3->SR & TIM_IT_CC1)
    {
        if( GPIOB->IDR & GPIO_Pin_4 )
        {
            channel5Rise = TIM3->CCR1;
            TIM3->CCER |= TIM_CCER_CC1P;
        }
        else
        {
            temp = TIM3->CCR1 - channel5Rise;
            TIM3->CCER &= ~TIM_CCER_CC1P;
            if( temp < 0 ) temp += 0xFFFF;
            channel5 = temp;
        }
        TIM3->SR &= ~(TIM_IT_CC1);
    }

    // channel3 -> CHANNEL 2, PA4
    if( TIM3->SR & TIM_IT_CC2)
    {
        if( GPIOA->IDR & GPIO_Pin_4 )
        {
            channel3Rise = TIM3->CCR2;
            TIM3->CCER |= TIM_CCER_CC2P;
        }
        else
        {
            temp = TIM3->CCR2 - channel3Rise;
            TIM3->CCER &= ~TIM_CCER_CC2P;
            if( temp < 0 ) temp += 0xFFFF;
            channel3 = temp;
        }
        TIM3->SR &= ~(TIM_IT_CC2);
    }
}

} // extern "C"
