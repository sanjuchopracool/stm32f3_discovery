#include "LEDNotification.h"
#include "stm32f30x.h"
#include "system.h"

// variables

const uint32_t allLeds = GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8;
const uint32_t greenLeds = GPIO_Pin_15 | GPIO_Pin_11;
const uint32_t orangeLeds = GPIO_Pin_14 | GPIO_Pin_10;
const uint32_t redLeds =  GPIO_Pin_13 |GPIO_Pin_9;
const uint32_t blueLeds =  GPIO_Pin_12 |GPIO_Pin_8;
uint32_t currentLeds = greenLeds;
uint16_t currentBlinkCount = 50;

GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
uint8_t currentError = NO_ERROR;

void initLeds()
{
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8);
    GPIO_Init(GPIOE, &GPIO_InitStructure);
}

void setError( uint8_t inError )
{
    if ( inError != currentError )
    {
        currentError = inError;
        GPIO_ResetBits( GPIOE, allLeds );

        switch (inError)
        {
        case NO_ERROR:
            currentLeds = greenLeds;
            currentBlinkCount = 50;
            break;
        case SETUP_ERROR:
            currentLeds = redLeds;
            currentBlinkCount = 25;
            break;
        case LOW_BATTERY:
            currentLeds = allLeds;
            currentBlinkCount = 25;
            break;
        default:
            currentLeds = orangeLeds | blueLeds;
            currentBlinkCount = 50;
            break;
        }
    }
}

void toggleLED()
{
    if(isSet)
        GPIO_ResetBits( GPIOE, currentLeds );
    else
        GPIO_SetBits( GPIOE, currentLeds );
    isSet = !isSet;
}

bool errorFlag;
void error()
{
    while(1)
    {
        if(errorFlag)
        {
            //            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
            GPIO_ResetBits( GPIOA, GPIO_Pin_1 );
        }
        else
        {
            //            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
            GPIO_SetBits( GPIOA, GPIO_Pin_1 );
        }
        errorFlag = !errorFlag;
        delay( 2 );
    }
}
