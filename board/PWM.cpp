#include "PWM.h"

#ifdef STM32F3DISCOVERY
#include <stm32f30x.h>
#include <stm32f30x_tim.h>

#define PWM_TIMER_CLK_ENABLE()                RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE)
#define PWM_TIMER_CLK_DISABLE()               RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, DISABLE)

#define PWM_TIMER_FORCE_RESET()               RCC_APB1PeriphResetCmd( RCC_APB1Periph_TIM4, ENABLE )
#define PWM_TIMER_RELEASE_RESET()             RCC_APB1PeriphResetCmd( RCC_APB1Periph_TIM4, DISABLE )

#define PWM_GPIO_CLK_ENABLE()                 RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD, ENABLE)
#define PWM_CHANNEL_1_PIN                     GPIO_Pin_12
#define PWM_CHANNEL_2_PIN                     GPIO_Pin_13
#define PWM_CHANNEL_3_PIN                     GPIO_Pin_14
#define PWM_CHANNEL_4_PIN                     GPIO_Pin_15
#define PWM_CHANNEL_1_PIN_SOURCE              GPIO_PinSource12
#define PWM_CHANNEL_2_PIN_SOURCE              GPIO_PinSource13
#define PWM_CHANNEL_3_PIN_SOURCE              GPIO_PinSource14
#define PWM_CHANNEL_4_PIN_SOURCE              GPIO_PinSource15
#define PWM_PIN_PORT                          GPIOD
#define PWM_PIN_AF_MODE                       GPIO_AF_2
#endif

//
TIM_OCInitTypeDef  pwmInitStructure = {};
void initPwm()
{
    //enable GPIO clock
    PWM_GPIO_CLK_ENABLE();
    // enable pins
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = PWM_CHANNEL_1_PIN | PWM_CHANNEL_2_PIN | PWM_CHANNEL_3_PIN | PWM_CHANNEL_4_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
    GPIO_Init(PWM_PIN_PORT, &GPIO_InitStructure);

    GPIO_PinAFConfig(PWM_PIN_PORT, PWM_CHANNEL_1_PIN_SOURCE, PWM_PIN_AF_MODE);
    GPIO_PinAFConfig(PWM_PIN_PORT, PWM_CHANNEL_2_PIN_SOURCE, PWM_PIN_AF_MODE);
    GPIO_PinAFConfig(PWM_PIN_PORT, PWM_CHANNEL_3_PIN_SOURCE, PWM_PIN_AF_MODE);
    GPIO_PinAFConfig(PWM_PIN_PORT, PWM_CHANNEL_4_PIN_SOURCE, PWM_PIN_AF_MODE);

    // enable timer4 clock, base frequency at 72MHz
    PWM_TIMER_CLK_ENABLE();

    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Prescaler = 71;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = 5000;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
    TIM_Cmd(TIM4, ENABLE);

    // pwm init structure
    pwmInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    pwmInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    pwmInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    setPwm(CHANNEL1, 1000);
    setPwm(CHANNEL2, 1000);
    setPwm(CHANNEL3, 1000);
    setPwm(CHANNEL4, 1000);
}

void setPwm(PWM_CHANNEL inChannel, float dutyCycle)
{
    pwmInitStructure.TIM_Pulse = dutyCycle;
    switch ( inChannel ) {
    case CHANNEL1:
    {
        TIM_OC1Init(TIM4, &pwmInitStructure);
        TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    }
    case CHANNEL2:
    {
        TIM_OC2Init(TIM4, &pwmInitStructure);
        TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    }
    case CHANNEL3:
    {
        TIM_OC3Init(TIM4, &pwmInitStructure);
        TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    }
    case CHANNEL4:
    {
        TIM_OC4Init(TIM4, &pwmInitStructure);
        TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);
        break;
    }
    }
}
