/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"
#include "system.h"
#include "serial.h"
#include "SPI.h"
#include "L3GD20Gyro.h"
#include "Receiver.h"
#include "VoltageReader.h"
#include "PWM.h"

enum Error
{
    NO_ERROR,           // Green
    SETUP_ERROR,        // ALL
    LOW_BATTERY,        // Red
    RECEIVER_ERROR,     // Orange
    LOOP_TIME_EXPIRED,  // Orange
};

// MACROS
///////////////////////////////////////////////////////////////////////////////

// variables
GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
uint8_t theError = Error::NO_ERROR;

const uint32_t allLeds = GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8;
const uint32_t greenLeds = GPIO_Pin_15 | GPIO_Pin_11;
const uint32_t orangeLeds = GPIO_Pin_14 | GPIO_Pin_10;
const uint32_t redLeds =  GPIO_Pin_13 |GPIO_Pin_9;
const uint32_t blueLeds =  GPIO_Pin_12 |GPIO_Pin_8;
uint32_t currentLeds = greenLeds;
uint16_t currentBlinkCount = 125;

// Gyro
int16_t theGyroValue[3];
//int16_t gyroOffset[3];

///////////////////////////////////////////////////////////////////////////////

void setError( uint8_t inError )
{
    theError = inError;
    GPIO_ResetBits( GPIOE, allLeds );

    switch (inError)
    {
    case Error::NO_ERROR:
        currentLeds = greenLeds;
        currentBlinkCount = 50;
        break;
    case Error::SETUP_ERROR:
        currentLeds = redLeds;
        currentBlinkCount = 25;
        break;
    case Error::LOW_BATTERY:
        currentLeds = allLeds;
        currentBlinkCount = 25;
        break;
    default:
        currentLeds = orangeLeds | blueLeds;
        currentBlinkCount = 50;
        break;
    }
}

void toggleLED()
{
    if(isSet)
        GPIO_ResetBits( GPIOE, currentLeds );
    else
        GPIO_SetBits( GPIOE, currentLeds );
    isSet = !isSet;
}

bool errorFlag;
void error()
{
    while(1)
    {
        if(errorFlag)
        {
//            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
            GPIO_ResetBits( GPIOA, GPIO_Pin_1 );
        }
        else
        {
//            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
            GPIO_SetBits( GPIOA, GPIO_Pin_1 );
        }
        errorFlag = !errorFlag;
        delay( 2 );
    }
}

extern volatile int32_t channel1;  //PA2
extern volatile int32_t channel2;  //PA3
extern volatile int32_t channel3;  //PA4
extern volatile int32_t channel4;  //PA5
extern volatile int32_t channel5;  //PA6

void TIM_Config(void);
int main(void)
{
    systemInit(); // it will start sysTick
    initSerial(); // 115200 baud rate PA9, PA10

    printf( "Hello World\n" );

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    //enable interrupt for user button
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init( GPIOA, &GPIO_InitStructure );

    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
    EXTI_InitTypeDef   EXTI_InitStructure;
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set Button EXTI Interrupt to the lowest priority */
    NVIC_InitTypeDef   NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (0x0F - 1);
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);


    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8);
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    L3GD20Gyro theGyro(SPIInterface::SPI_1);
    initReceiver();
    initPwm();
    VoltageReader theVoltageReader;

    uint64_t loopTime = micros();
    uint8_t activeChannel = 1;

    uint32_t prevChannelalue = channel4;

    // Actual Setup
    ///////////////////////////////////////////////////////////////
    uint32_t loopCount = 0;
    volatile int32_t& throttle = channel3;
    volatile int32_t& yaw = channel5;
    volatile int32_t& pitch = channel1;
    volatile int32_t& roll = channel2;
    volatile int32_t& button = channel4;
    ///////////////////////////////////////////////////////////////
    int16_t delta = 2000;
    while(1)
    {
        if ( micros() - loopTime > 4050 ) theError = Error::LOOP_TIME_EXPIRED;
        while ( micros() - loopTime  < 4000);
        loopTime = micros();

        if ( channel3 > 1150 )
            delta = 1000;

        printf("throttle on All Channel %d\n",delta);

        TIM4->CCR1 = delta;
        TIM4->CCR2 = delta;
        TIM4->CCR3 = delta;
        TIM4->CCR4 = delta;
        TIM4->CNT = 5000;
    }
}

extern "C"
{
void EXTI0_IRQHandler()
{
    toggleLED();
    EXTI_ClearITPendingBit(EXTI_Line0);
}
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif
