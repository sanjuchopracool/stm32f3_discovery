/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"
#include "system.h"
#include "serial.h"
#include "SPI.h"
#include "L3GD20Gyro.h"
#include "Receiver.h"
#include "VoltageReader.h"
#include "PWM.h"
#include "LEDNotification.h"
#include "PID.h"

// external variables
///////////////////////////////////////////////////////////////////////////////
extern uint16_t currentBlinkCount;
extern float pid_output_pitch;
extern float pid_output_roll;
extern float pid_output_yaw;
extern float pid_i_mem_yaw;

// Gyro
///////////////////////////////////////////////////////////////////////////////
int16_t theGyroValue[3];
int16_t gyroOffset[3] = { -33, -31, 19 };
float gyro_pitch_input;
float gyro_roll_input;
float gyro_yaw_input;
float receiver_pitch_input;
float receiver_roll_input;
float receiver_yaw_input;

constexpr float sensitivity = 57.14286f;
constexpr float gyroFilterConstant1 = 0.8f;
constexpr float gyroFilterConstant2 = 1.0f - gyroFilterConstant1;
///////////////////////////////////////////////////////////////////////////////

// Battery
///////////////////////////////////////////////////////////////////////////////
float battery_Voltage_inV = 0.0f;
constexpr float diodeOffset = 1.18f;
// 4.045555 = 10.56/2.61
// (4.046/1408.62745 = 0.002873
constexpr float voltageFactor = 0.002873f;
uint16_t battery_voltage;
///////////////////////////////////////////////////////////////////////////////


extern volatile int32_t channel1;  //PA2
extern volatile int32_t channel2;  //PA3
extern volatile int32_t channel3;  //PA4
extern volatile int32_t channel4;  //PA5
extern volatile int32_t channel5;  //PA6

void TIM_Config(void);
int main(void)
{
    {
        systemInit(); // it will start sysTick
        initSerial(); // 115200 baud rate PA9, PA10
        initReceiver();
        initPwm();
        initLeds();
    }

    L3GD20Gyro theGyro(SPIInterface::SPI_1);
    VoltageReader theVoltageReader;


    // Actual Setup
    ///////////////////////////////////////////////////////////////
    volatile int32_t& throttleSignal = channel3;
    volatile int32_t& yawSignal = channel5;
    volatile int32_t& pitchSignal = channel1;
    volatile int32_t& rollSignal = channel2;
    volatile int32_t& switchInput = channel4;
    ///////////////////////////////////////////////////////////////

    setError( NO_ERROR );
    // calibrate Gyro
    ///////////////////////////////////////////////////////////////
    {
        printf( "Gyro Calibration\n" );
        int32_t gyroCalibration[3] = {};
        for( uint16_t i = 0; i < 2000; ++i )
        {
            theGyro.readRawRate(theGyroValue);
            gyroCalibration[0] += theGyroValue[0];
            gyroCalibration[1] += theGyroValue[1];
            gyroCalibration[2] += theGyroValue[2];
            delayMicroseconds(4000);
            if ( !(i % 25) ) toggleLED();
        }

        gyroOffset[0] = gyroCalibration[0]/2000;
        gyroOffset[1] = gyroCalibration[1]/2000;
        gyroOffset[2] = gyroCalibration[2]/2000;

//        printf( "Calibration Value: %d %d %d\n", gyroOffset[0], gyroOffset[1], gyroOffset[2] );
    }

//    auto waitForStartUp = [&throttleSignal, &switchInput]() {
//        // initally throttle input must be in low position
//        while( throttleSignal > 1050 || throttleSignal < 950 );
//        // initally switchInput input must be in low position
//        while( switchInput > 1050 || switchInput < 950 );
//        // and then should be in high position
//        while( switchInput < 1950 );
//    };

//    waitForStartUp();

    bool started = true;
    float throttle = throttleSignal;
    uint32_t esc_1 = 0;
    uint32_t esc_2 = 0;
    uint32_t esc_3 = 0;
    uint32_t esc_4 = 0;
    battery_Voltage_inV = voltageFactor * theVoltageReader.readVoltage() + diodeOffset;

    // loop time and count
    ///////////////////////////////////////////////////////////////
    uint32_t loopCount = 0;
    uint64_t loopTime = micros();
    while(1)
    {
        ///////////////////////////////////////////////////////////////
        // Error Handling
        loopCount++;
        if (!( loopCount % currentBlinkCount ))
            toggleLED();
        ///////////////////////////////////////////////////////////////

        // Voltage
        ///////////////////////////////////////////////////////////////
        battery_Voltage_inV = battery_Voltage_inV * 0.92f + (voltageFactor * theVoltageReader.readVoltage() + diodeOffset)*0.08f;
        if ( battery_Voltage_inV < 10.80f )
            setError( LOW_BATTERY );
        ///////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////
        theGyro.readRawRate(theGyroValue);
        theGyroValue[0] -= gyroOffset[0];
        theGyroValue[1] -= gyroOffset[1];
        theGyroValue[2] -= gyroOffset[2];

        // invert yaw
        theGyroValue[2] *= -1;

        gyro_pitch_input = (gyro_pitch_input * gyroFilterConstant1) + (((float)theGyroValue[0]/sensitivity)*gyroFilterConstant2);
        gyro_roll_input = (gyro_roll_input * gyroFilterConstant1) + (((float)theGyroValue[1]/sensitivity)*gyroFilterConstant2);
        gyro_yaw_input = (gyro_yaw_input * gyroFilterConstant1) + (((float)theGyroValue[2]/sensitivity)*gyroFilterConstant2);
        ///////////////////////////////////////////////////////////////

        throttle = throttleSignal;

        // if running and switch go low
        if ( started  && ( switchInput > 950 && switchInput < 1050 ) )
            started = false;

        if ( !started  && ( switchInput > 1950 ) && ( throttle > 950 && throttle < 1050 ))
        {
            started = true;
            resetPID();
        }

        //The PID set point in degrees per second is determined by the roll receiver input.
        //In the case of deviding by 3 the max roll rate is aprox 164 degrees per second ( (500-8)/3 = 164d/s ).
        receiver_roll_input = 0;
        //We need a little dead band of 16us for better results.
        if(rollSignal > 1530)receiver_roll_input = (rollSignal - 1530)/3.0;
        else if(rollSignal < 1470)receiver_roll_input = (rollSignal - 1470)/3.0;

        //The PID set point in degrees per second is determined by the pitch receiver input.
        //In the case of deviding by 3 the max pitch rate is aprox 164 degrees per second ( (500-8)/3 = 164d/s ).
        receiver_pitch_input = 0;
        //We need a little dead band of 16us for better results.
        if(pitchSignal > 1530)receiver_pitch_input = (pitchSignal - 1530)/3.0;
        else if(pitchSignal < 1470)receiver_pitch_input = (pitchSignal - 1470)/3.0;


        receiver_yaw_input = 0;
        if(yawSignal > 1050)
        { // My receiver have issue with yaw input
          if(yawSignal > 1600)receiver_yaw_input = (yawSignal - 1600)/5.0;
          else if(yawSignal < 1400)receiver_yaw_input = (yawSignal - 1400)/5.0;
        }

        calculatePID();

        if (started)
        {                                                          //The motors are started.
            if (throttle > 1800) throttle = 1800;                                   //We need some room to keep full control at full throttle.
            esc_1 = throttle - pid_output_pitch - pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 4 (front-left - CW)
            esc_2 = throttle - pid_output_pitch + pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 1 (front-right - CCW)
            esc_3 = throttle + pid_output_pitch + pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 2 (rear-right - CW)
            esc_4 = throttle + pid_output_pitch - pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 3 (rear-left - CCW)

            battery_voltage = battery_Voltage_inV*100;

            if (battery_voltage < 1240 && battery_voltage > 800)
            {                   //Is the battery connected?
                esc_1 += esc_1 * ((1240 - battery_voltage)/(float)3500);              //Compensate the esc-1 pulse for voltage drop.
                esc_2 += esc_2 * ((1240 - battery_voltage)/(float)3500);              //Compensate the esc-2 pulse for voltage drop.
                esc_3 += esc_3 * ((1240 - battery_voltage)/(float)3500);              //Compensate the esc-3 pulse for voltage drop.
                esc_4 += esc_4 * ((1240 - battery_voltage)/(float)3500);              //Compensate the esc-4 pulse for voltage drop.
            }

            if (esc_1 < 1050) esc_1 = 1050;                                         //Keep the motors running.
            if (esc_2 < 1050) esc_2 = 1050;                                         //Keep the motors running.
            if (esc_3 < 1050) esc_3 = 1050;                                         //Keep the motors running.
            if (esc_4 < 1050) esc_4 = 1050;                                         //Keep the motors running.

            if(esc_1 > 2000)esc_1 = 2000;                                           //Limit the esc-1 pulse to 2000us.
            if(esc_2 > 2000)esc_2 = 2000;                                           //Limit the esc-2 pulse to 2000us.
            if(esc_3 > 2000)esc_3 = 2000;                                           //Limit the esc-3 pulse to 2000us.
            if(esc_4 > 2000)esc_4 = 2000;                                           //Limit the esc-4 pulse to 2000us.
        }

        else
        {
            esc_1 = 1000;                                                           //If start is not 2 keep a 1000us pulse for ess-1.
            esc_2 = 1000;                                                           //If start is not 2 keep a 1000us pulse for ess-2.
            esc_3 = 1000;                                                           //If start is not 2 keep a 1000us pulse for ess-3.
            esc_4 = 1000;                                                           //If start is not 2 keep a 1000us pulse for ess-4.
        }

        TIM4->CCR1 = esc_1;
        TIM4->CCR2 = esc_2;
        TIM4->CCR3 = esc_3;
        TIM4->CCR4 = esc_4;
        TIM4->CNT = 5000;
        printf( "%4d %4d %4d %4d %4d %4d %4d\n",(int16_t)yawSignal, (int16_t)pid_i_mem_yaw, (int16_t)pid_output_yaw, esc_1, esc_2, esc_3, esc_4);
        ///////////////////////////////////////////////////////////////
        if ( micros() - loopTime > 4050 )
            setError( LOOP_TIME_EXPIRED );

        while ( micros() - loopTime  < 4000);
        loopTime = micros();
        ///////////////////////////////////////////////////////////////
    }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.s
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif
