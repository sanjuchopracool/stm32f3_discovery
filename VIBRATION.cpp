/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"
#include "system.h"
#include "serial.h"
#include "SPI.h"
#include "L3GD20Gyro.h"
#include "Receiver.h"
#include "VoltageReader.h"
#include "PWM.h"
#include <math.h>
#include "stm32f3_discovery_lsm303dlhc.h"

enum Error
{
    NO_ERROR,           // Green
    SETUP_ERROR,        // ALL
    LOW_BATTERY,        // Red
    RECEIVER_ERROR,     // Orange
    LOOP_TIME_EXPIRED,  // Orange
};

// MACROS
///////////////////////////////////////////////////////////////////////////////

// variables
GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
uint8_t currentError = NO_ERROR;

const uint32_t allLeds = GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8;
const uint32_t greenLeds = GPIO_Pin_15 | GPIO_Pin_11;
const uint32_t orangeLeds = GPIO_Pin_14 | GPIO_Pin_10;
const uint32_t redLeds =  GPIO_Pin_13 |GPIO_Pin_9;
const uint32_t blueLeds =  GPIO_Pin_12 |GPIO_Pin_8;
uint32_t currentLeds = greenLeds;
uint16_t currentBlinkCount = 50;

// Gyro
///////////////////////////////////////////////////////////////////////////////
int16_t theGyroValue[3];
int16_t gyroOffset[3] = { -33, -31, 19 };
float gyroPitchInput = 0.0f;
float gyroRollInput = 0.0f;
float gyroYawInput = 0.0f;

constexpr float sensitivity = 57.14286f;
constexpr float gyroFilterConstant1 = 0.8f;
constexpr float gyroFilterConstant2 = 1.0f - gyroFilterConstant1;
///////////////////////////////////////////////////////////////////////////////

// Battery
///////////////////////////////////////////////////////////////////////////////
float battery_Voltage_inV = 0.0f;
constexpr float diodeOffset = 1.18f;
// 4.045555 = 10.56/2.61
// (4.046/1408.62745 = 0.002873
constexpr float voltageFactor = 0.002873f;
///////////////////////////////////////////////////////////////////////////////

void setError( uint8_t inError )
{
    if ( inError != currentError )
    {
        currentError = inError;
        GPIO_ResetBits( GPIOE, allLeds );

        switch (inError)
        {
        case NO_ERROR:
            currentLeds = greenLeds;
            currentBlinkCount = 50;
            break;
        case SETUP_ERROR:
            currentLeds = redLeds;
            currentBlinkCount = 25;
            break;
        case LOW_BATTERY:
            currentLeds = allLeds;
            currentBlinkCount = 25;
            break;
        default:
            currentLeds = orangeLeds | blueLeds;
            currentBlinkCount = 50;
            break;
        }
    }
}

float AccBuffer[3] = {0.0f};
#define LSM_Acc_Sensitivity_2g     (float)     1.0f            /*!< accelerometer sensitivity with 2 g full scale [LSB/mg] */
#define LSM_Acc_Sensitivity_4g     (float)     0.5f            /*!< accelerometer sensitivity with 4 g full scale [LSB/mg] */
#define LSM_Acc_Sensitivity_8g     (float)     0.25f           /*!< accelerometer sensitivity with 8 g full scale [LSB/mg] */
#define LSM_Acc_Sensitivity_16g    (float)     0.0834f         /*!< accelerometer sensitivity with 12 g full scale [LSB/mg] */
void Demo_CompassReadAcc(float* pfData)
{
    int16_t pnRawData[3];
    uint8_t ctrlx[2];
    uint8_t buffer[6], cDivider;
    uint8_t i = 0;
    float LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_2g;

    /* Read the register content */
    LSM303DLHC_Read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG4_A, ctrlx,2);
    LSM303DLHC_Read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_X_L_A, buffer, 6);

    if(ctrlx[1]&0x40)
        cDivider=64;
    else
        cDivider=16;

    /* check in the control register4 the data alignment*/
    if(!(ctrlx[0] & 0x40) || (ctrlx[1] & 0x40)) /* Little Endian Mode or FIFO mode */
    {
        for(i=0; i<3; i++)
        {
            pnRawData[i]=((int16_t)((uint16_t)buffer[2*i+1] << 8) + buffer[2*i])/cDivider;
        }
    }
    else /* Big Endian Mode */
    {
        for(i=0; i<3; i++)
            pnRawData[i]=((int16_t)((uint16_t)buffer[2*i] << 8) + buffer[2*i+1])/cDivider;
    }
    /* Read the register content */
    LSM303DLHC_Read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG4_A, ctrlx,2);


    if(ctrlx[1]&0x40)
    {
        /* FIFO mode */
        LSM_Acc_Sensitivity = 0.25;
    }
    else
    {
        /* normal mode */
        /* switch the sensitivity value set in the CRTL4*/
        switch(ctrlx[0] & 0x30)
        {
        case LSM303DLHC_FULLSCALE_2G:
            LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_2g;
            break;
        case LSM303DLHC_FULLSCALE_4G:
            LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_4g;
            break;
        case LSM303DLHC_FULLSCALE_8G:
            LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_8g;
            break;
        case LSM303DLHC_FULLSCALE_16G:
            LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_16g;
            break;
        }
    }

    /* Obtain the mg value for the three axis */
    for(i=0; i<3; i++)
    {
        pfData[i]=(float)pnRawData[i]/LSM_Acc_Sensitivity;
    }

}

void toggleLED()
{
    if(isSet)
        GPIO_ResetBits( GPIOE, currentLeds );
    else
        GPIO_SetBits( GPIOE, currentLeds );
    isSet = !isSet;
}

bool errorFlag;
void error()
{
    while(1)
    {
        if(errorFlag)
        {
            //            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
            GPIO_ResetBits( GPIOA, GPIO_Pin_1 );
        }
        else
        {
            //            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
            GPIO_SetBits( GPIOA, GPIO_Pin_1 );
        }
        errorFlag = !errorFlag;
        delay( 2 );
    }
}

extern volatile int32_t channel1;  //PA2
extern volatile int32_t channel2;  //PA3
extern volatile int32_t channel3;  //PA4
extern volatile int32_t channel4;  //PA5
extern volatile int32_t channel5;  //PA6

void TIM_Config(void);
int main(void)
{
    systemInit(); // it will start sysTick
    initSerial(); // 115200 baud rate PA9, PA10

    printf( "Hello World\n" );

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE );
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    //enable interrupt for user button
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init( GPIOA, &GPIO_InitStructure );

    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
    EXTI_InitTypeDef   EXTI_InitStructure;
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set Button EXTI Interrupt to the lowest priority */
    NVIC_InitTypeDef   NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (0x0F - 1);
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);


    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8);
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    L3GD20Gyro theGyro(SPIInterface::SPI_1);
    initReceiver();
    initPwm();
    VoltageReader theVoltageReader;

        // setup accelerometer
        LSM303DLHCAcc_InitTypeDef LSM303DLHCAcc_InitStructure;
        LSM303DLHCAcc_FilterConfigTypeDef LSM303DLHCFilter_InitStructure;

        /* Fill the accelerometer structure */
        LSM303DLHCAcc_InitStructure.Power_Mode = LSM303DLHC_NORMAL_MODE;
        LSM303DLHCAcc_InitStructure.AccOutput_DataRate = LSM303DLHC_ODR_50_HZ;
        LSM303DLHCAcc_InitStructure.Axes_Enable= LSM303DLHC_AXES_ENABLE;
        LSM303DLHCAcc_InitStructure.AccFull_Scale = LSM303DLHC_FULLSCALE_2G;
        LSM303DLHCAcc_InitStructure.BlockData_Update = LSM303DLHC_BlockUpdate_Continous;
        LSM303DLHCAcc_InitStructure.Endianness=LSM303DLHC_BLE_LSB;
        LSM303DLHCAcc_InitStructure.High_Resolution=LSM303DLHC_HR_ENABLE;
        /* Configure the accelerometer main parameters */
        LSM303DLHC_AccInit(&LSM303DLHCAcc_InitStructure);

        /* Fill the accelerometer LPF structure */
        LSM303DLHCFilter_InitStructure.HighPassFilter_Mode_Selection =LSM303DLHC_HPM_NORMAL_MODE;
        LSM303DLHCFilter_InitStructure.HighPassFilter_CutOff_Frequency = LSM303DLHC_HPFCF_16;
        LSM303DLHCFilter_InitStructure.HighPassFilter_AOI1 = LSM303DLHC_HPF_AOI1_DISABLE;
        LSM303DLHCFilter_InitStructure.HighPassFilter_AOI2 = LSM303DLHC_HPF_AOI2_DISABLE;

        /* Configure the accelerometer LPF main parameters */
        LSM303DLHC_AccFilterConfig(&LSM303DLHCFilter_InitStructure);



    // Actual Setup
    ///////////////////////////////////////////////////////////////
    volatile int32_t& throttleSignal = channel3;
    volatile int32_t& yawSignal = channel5;
    volatile int32_t& pitchSignal = channel1;
    volatile int32_t& rollSignal = channel2;
    volatile int32_t& switchInput = channel4;
    ///////////////////////////////////////////////////////////////

//    auto waitForStartUp = [&throttleSignal, &switchInput]() {
//        // initally throttle input must be in low position
//        while( throttleSignal > 1050 || throttleSignal < 950 );
//        // initally switchInput input must be in low position
//        while( switchInput > 1050 || switchInput < 950 );
//        // and then should be in high position
//        while( switchInput < 1950 );
//    };

//    waitForStartUp();

    bool started = true;
    float throttle = throttleSignal;
    setError( NO_ERROR );
    battery_Voltage_inV = voltageFactor * theVoltageReader.readVoltage() + diodeOffset;

    // loop time and count
    ///////////////////////////////////////////////////////////////
    uint32_t loopCount = 0;
    uint64_t loopTime = micros();
    int8_t currrentChannel = 1;
    int32_t vibration_array[20] = {}, avarage_vibration_level = 0, vibration_total_result = 0;
    uint8_t array_counter, vibration_counter = 0;
    while(1)
    {
        ///////////////////////////////////////////////////////////////
        // Error Handling
        loopCount++;
        if (!( loopCount % currentBlinkCount ))
            toggleLED();
        ///////////////////////////////////////////////////////////////

        // Voltage
        ///////////////////////////////////////////////////////////////
        battery_Voltage_inV = battery_Voltage_inV * 0.92f + (voltageFactor * theVoltageReader.readVoltage() + diodeOffset)*0.08f;
        if ( battery_Voltage_inV < 10.80f )
            setError( LOW_BATTERY );
        ///////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////
        Demo_CompassReadAcc(AccBuffer);
        vibration_array[0] = sqrt( AccBuffer[0]*AccBuffer[0]
                + AccBuffer[1]*AccBuffer[1]
                + AccBuffer[2]*AccBuffer[2] )/10;

        for (array_counter = 16; array_counter > 0; array_counter--) {                   //Do this loop 16 times to create an array of accelrometer vectors.
            vibration_array[array_counter] = vibration_array[array_counter - 1];           //Shift every variable one position up in the array.
            avarage_vibration_level += vibration_array[array_counter];                     //Add the array value to the acc_av_vector variable.
        }
        avarage_vibration_level /= 17;                                                   //Divide the acc_av_vector by 17 to get the avarage total accelerometer vector.

        if (vibration_counter < 100) {                                                    //If the vibration_counter is less than 20 do this.
            vibration_counter ++;                                                          //Increment the vibration_counter variable.
            vibration_total_result += abs(vibration_array[0] - avarage_vibration_level);   //Add the absolute difference between the avarage vector and current vector to the vibration_total_result variable.
        }
        else {
            vibration_counter = 0;                                                         //If the vibration_counter is equal or larger than 20 do this.
            printf("%d\n",vibration_total_result);
            vibration_total_result = 0;                                                    //Reset the vibration_total_result variable.
        }


        throttle = throttleSignal;

        // if running and switch go low
        if ( started  && ( switchInput > 950 && switchInput < 1050 ) )
            started = false;

        if ( !started  && ( switchInput > 1950 ) && ( throttle > 950 && throttle < 1050 ))
        {
            started = true;
            currrentChannel++;

            if ( currrentChannel > 4 )
                currrentChannel = 1;
        }

        if ( !started )
            throttle = 1000;

        if( throttle < 1000 || throttle > 2000 )
            throttle = 1000;

        TIM4->CCR1 = (1 == currrentChannel) ? throttle : 1000;
        TIM4->CCR2 = (2 == currrentChannel) ? throttle : 1000;
        TIM4->CCR3 = (3 == currrentChannel) ? throttle : 1000;
        TIM4->CCR4 = (4 == currrentChannel) ? throttle : 1000;
        TIM4->CNT = 5000;

        ///////////////////////////////////////////////////////////////
        if ( micros() - loopTime > 4050 )
            setError( LOOP_TIME_EXPIRED );

        while ( micros() - loopTime  < 4000);
        loopTime = micros();
        ///////////////////////////////////////////////////////////////
    }
}

extern "C"
{
void EXTI0_IRQHandler()
{
    toggleLED();
    EXTI_ClearITPendingBit(EXTI_Line0);
}

uint32_t LSM303DLHC_TIMEOUT_UserCallback ()
{
    while ( 1 )
    {
        printf((char*)"I2c timeout\n");
    }
    return 0;
}
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif
